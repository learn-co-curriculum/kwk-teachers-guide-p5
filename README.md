## Objectives

1. Students will get an introduction to the p5 JavaScript library
2. Students will practice writing JavaScript in conjunction with canvas

## Resources

* [p5 Tutorials](https://p5js.org/learn/)
* [Overview of p5 features](https://github.com/processing/p5.js/wiki/p5.js-overview)
* [Canvas reference](https://www.google.com/search?q=canvas+intro&rlz=1C5CHFA_enUS757US757&oq=canvas+intro&aqs=chrome..69i57j0l5.2054j0j1&sourceid=chrome&ie=UTF-8)
